﻿using System;
using System.Collections.Generic;
using System.Linq;

/**
 * Send your busters out into the fog to trap ghosts and bring them home!
 **/

namespace CodeBusters
{
    public enum EntityTypes
    {
        Ghost = -1,
        Ally,
        Enemy
    }

    public interface IEntity
    {
        void Update(Coordinates coordinates, int state, int value);

        bool Visible { get; set; }
        Coordinates Coordinates { get; set; }
    }

    public abstract class State
    {
        public abstract void Handle(Buster buster);
    }

    class Explore : State
    {
        public Explore(GameState gameState)
        {
            _gameState = gameState;
        }

        private readonly GameState _gameState;

        private Coordinates _targetCoordinates;

        public Area AreaToVisit { get; private set; }

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("Explore");

            if (buster.Stunned)
            {
                Console.Error.WriteLine("Explore -> Stunned");
                buster.State = new Stunned(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.ShouldStun(buster))
            {
                Buster target = _gameState.StunTarget(buster);
                buster.Stun(target);
                buster.State = new Explore(_gameState);
                return;
            }

            if (_gameState.CarryingEnemyVisible() && buster.CanUseStun())
            {
                Console.Error.WriteLine("AreYouStillThere -> ChaseEnemyCarryingGhost");
                Buster enemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true);
                buster.State = new ChaseEnemyCarryingGhost(_gameState, enemy);
                buster.MakeMove(_gameState);
                return;
            }

            if (!buster.UsedRadar && buster.Coordinates.Distance(_gameState.BaseCoordinates) > 3000)
            {
                int unexploredNearbyAreas = 0;
                foreach (Area area in _gameState.Map.Areas)
                {
                    if (buster.Coordinates.Distance(area) < 3500 && !area.Visited)
                        unexploredNearbyAreas++;
                }
                if (unexploredNearbyAreas >= 2)
                {
                    buster.Radar(_gameState);
                    return;
                }
            }

            if (_gameState.EntityVisible(EntityTypes.Ghost))
            {
                Console.Error.WriteLine("Explore -> ChaseGhost");
                Ghost target = (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost);
                buster.State = new ChaseGhost(_gameState, target, buster);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.BustingEnemyVisible())
            {
                _targetCoordinates = _gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCapturingGhost: true).Coordinates;
                Console.Error.WriteLine("Explore: BustingEnemyVisible");
            }

            else if (_gameState.GhostPositionRemembered())
            {
                Console.Error.WriteLine("Explore -> AreYouStillThere");
                Ghost target = (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost, stilThere: true);
                buster.State = new AreYouStillThere(_gameState, target, buster);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.Map.ExistsUnvisitedArea() && (AreaToVisit == null || AreaToVisit.Visited == true) && !_gameState.BustingEnemyVisible())
            {
                AreaToVisit = _gameState.Map.AreaToVisit(buster);
                _targetCoordinates = AreaToVisit;
                Console.Error.WriteLine("Explore: Unvisited Area ({0}, {1})", AreaToVisit.X, AreaToVisit.Y);
            }

            if (_targetCoordinates == null || _targetCoordinates.Distance(buster.Coordinates) < 50.0)
            {
                _targetCoordinates = Coordinates.Random(buster, _gameState.TurnNumber);
                AreaToVisit = null;
                Console.Error.WriteLine("Explore: Random");
            }
            buster.Move(_targetCoordinates);
        }
    }

    class ChaseGhost : State
    {
        private GameState _gameState;

        private Ghost _ghost;

        private double maxCatchRange = 1760;

        public ChaseGhost(GameState gameState, Ghost ghost, Buster buster)
        {
            _gameState = gameState;
            _ghost = ghost;
            _ghost.ChasedByBusters.Add(buster);
        }

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("ChaseGhost");

            if (buster.Stunned)
            {
                Console.Error.WriteLine("ChaseGhost -> Stunned");
                buster.State = new Stunned(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.ShouldStun(buster))
            {
                Buster target = _gameState.StunTarget(buster);
                buster.Stun(target);
                buster.State = new Explore(_gameState);
                return;
            }

            if (_gameState.CarryingEnemyVisible() && buster.CanUseStun())
            {
                Console.Error.WriteLine("AreYouStillThere -> ChaseEnemyCarryingGhost");
                Buster enemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true);
                buster.State = new ChaseEnemyCarryingGhost(_gameState, enemy);
                buster.MakeMove(_gameState);
                return;
            }

            if (!_ghost.Visible)
            {
                _ghost.ChasedByBusters.Remove(buster);
                if (!_gameState.EntityVisible(EntityTypes.Ghost))
                {
                    Console.Error.WriteLine("ChaseGhost -> Explore ({0} > {1} (maxCatchRange))", buster.Coordinates.Distance(_ghost.Coordinates), maxCatchRange);
                    buster.State = new Explore(_gameState);
                    buster.MakeMove(_gameState);
                    return;
                }
                else
                {
                    _ghost = (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost);
                    _ghost.ChasedByBusters.Add(buster);
                }
            }
            if (_gameState.EntityVisible(EntityTypes.Ghost) && _gameState.NearestEntity(buster, EntityTypes.Ghost) != _ghost &&
                _ghost.NumberOfAttackingBusters == 0)
            {
                _ghost.ChasedByBusters.Remove(buster);
                _ghost = (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost);
                _ghost.ChasedByBusters.Add(buster);
            }
            if (buster.Coordinates.Distance(_ghost.Coordinates) <= maxCatchRange)
            {
                _ghost.ChasedByBusters.Remove(buster);
                Console.Error.WriteLine("ChaseGhost -> Busting ({0} <= {1} (maxCatchRange))", buster.Coordinates.Distance(_ghost.Coordinates), maxCatchRange);
                buster.State = new Busting(_gameState, _ghost);
                buster.MakeMove(_gameState);
                return;
            }
            buster.Move(_ghost.Coordinates);
        }
    }

    class AreYouStillThere : State
    {
        public AreYouStillThere(GameState gameState, Ghost ghost, Buster buster)
        {
            _gameState = gameState;
            _ghost = ghost;
            _ghost.ChasedByBusters.Add(buster);
        }

        private readonly GameState _gameState;

        private Ghost _ghost;

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("AreYouStillThere");

            if (buster.Stunned)
            {
                Console.Error.WriteLine("AreYouStillThere -> Stunned");
                buster.State = new Stunned(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.ShouldStun(buster))
            {
                Buster target = _gameState.StunTarget(buster);
                buster.Stun(target);
                buster.State = new Explore(_gameState);
                return;
            }

            if (_gameState.CarryingEnemyVisible() && buster.CanUseStun())
            {
                Console.Error.WriteLine("AreYouStillThere -> ChaseEnemyCarryingGhost");
                Buster enemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true);
                buster.State = new ChaseEnemyCarryingGhost(_gameState, enemy);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.EntityVisible(EntityTypes.Ghost))
            {
                Console.Error.WriteLine("AreYouStillThere -> ChaseGhost");
                Ghost target = (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost);
                buster.State = new ChaseGhost(_gameState, target, buster);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.BustingEnemyVisible())
            {
                Console.Error.WriteLine("AreYouStillThere -> Explore");
                buster.State = new Explore(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (!_ghost.StillThere)
            {
                _ghost.ChasedByBusters.Remove(buster);
                if (!_gameState.GhostPositionRemembered())
                {
                    Console.Error.WriteLine("ChaseGhost -> Explore (No ghosts remembered)");
                    buster.State = new Explore(_gameState);
                    buster.MakeMove(_gameState);
                    return;
                }
                else
                {
                    _ghost = (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost, stilThere: true);
                    _ghost.ChasedByBusters.Add(buster);
                }
            }

            buster.Move(_ghost.Coordinates);
        }
    }

    class Busting : State
    {
        public Busting(GameState gameState, Ghost ghost)
        {
            _gameState = gameState;
            _ghost = ghost;
        }

        private GameState _gameState;

        private Ghost _ghost;

        private double maxCatchRange = 1760;
        private double minCatchRange = 900;

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("Busting");

            if (buster.Stunned)
            {
                Console.Error.WriteLine("Busting -> Stunned");
                buster.State = new Stunned(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.ShouldStun(buster))
            {
                Buster target = _gameState.StunTarget(buster);
                buster.Stun(target);
                buster.State = new Explore(_gameState);
                return;
            }

            if (buster.CarryingGhost && buster.CarriedGhostId == _ghost.Id)
            {
                Console.Error.WriteLine("Busting -> CarryingGhost");
                _ghost.StillThere = false;
                buster.State = new CarryingGhost(_gameState, _ghost);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.CarryingEnemyVisible() && buster.CanUseStun())
            {
                Console.Error.WriteLine("AreYouStillThere -> ChaseEnemyCarryingGhost");
                Buster enemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true);
                buster.State = new ChaseEnemyCarryingGhost(_gameState, enemy);
                buster.MakeMove(_gameState);
                return;
            }

            if (!_ghost.Visible)
            {
                _ghost.ChasedByBusters.Remove(buster);
                if (!_gameState.EntityVisible(EntityTypes.Ghost))
                {
                    Console.Error.WriteLine("Busting -> Explore");
                    buster.State = new Explore(_gameState);
                    buster.MakeMove(_gameState);
                }
                else
                {
                    Console.Error.WriteLine("Busting -> ChaseGhost");
                    buster.State = new ChaseGhost(_gameState, (Ghost)_gameState.NearestEntity(buster, EntityTypes.Ghost), buster);
                    buster.MakeMove(_gameState);
                }
                return;
            }

            if (buster.Coordinates.Distance(_ghost.Coordinates) > maxCatchRange)
            {
                Console.Error.WriteLine("Busting -> ChaseGhost ({0} > {1} (maxCatchRange))", buster.Coordinates.Distance(_ghost.Coordinates), maxCatchRange);
                buster.State = new ChaseGhost(_gameState, _ghost, buster);
                buster.MakeMove(_gameState);
                return;
            }

            if (buster.Coordinates.Distance(_ghost.Coordinates) < minCatchRange)
            {
                buster.Move(Coordinates.Random(buster, _gameState.TurnNumber));
                Console.Error.WriteLine("Busting: Random move ({0} < {1} (minCatchRange))", buster.Coordinates.Distance(_ghost.Coordinates), minCatchRange);
                return;
            }

            buster.Bust(_ghost);
        }
    }

    class CarryingGhost : State
    {
        public CarryingGhost(GameState gameState, Ghost ghost)
        {
            _gameState = gameState;
            // _ghost = ghost;
        }

        private readonly GameState _gameState;

        // private Ghost _ghost;
        
        private enum Edges
        {
            Vertical,
            Horizontal
        }

        private Edges selectedEdge;

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("CarryingGhost");

            if (buster.Stunned)
            {
                Console.Error.WriteLine("CarryingGhost -> Stunned");
                buster.State = new Stunned(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.ShouldStun(buster))
            {
                Buster target = _gameState.StunTarget(buster);
                buster.Stun(target);
                buster.State = new Explore(_gameState);
                return;
            }

            if (buster.Coordinates.Distance(_gameState.BaseCoordinates) > 1600)
            {
                if (Math.Abs(buster.Coordinates.X - _gameState.BaseCoordinates.X) <
                    Math.Abs(buster.Coordinates.Y - _gameState.BaseCoordinates.Y))
                    selectedEdge = Edges.Vertical;
                else
                    selectedEdge = Edges.Horizontal;

                /*if (_gameState.EntityVisible(EntityTypes.Enemy))
                {
                    Buster nearestEnemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy);
                    if (buster.Coordinates.Distance(nearestEnemy.Coordinates) < 2200 &&
                        buster.Coordinates.Distance(nearestEnemy.Coordinates) > 1760)
                    {
                        buster.Move(buster.Coordinates.Opposite(nearestEnemy.Coordinates));
                        Console.Error.WriteLine("CarryingGhost: Run Away");
                        return;
                    }
                }*/

                if (selectedEdge == Edges.Vertical)
                {
                    if (_gameState.BaseCoordinates.X != buster.Coordinates.X)
                        buster.Move(new Coordinates(_gameState.BaseCoordinates.X, buster.Coordinates.Y));
                    else
                        buster.Move(_gameState.BaseCoordinates);
                }
                else
                {
                    if (_gameState.BaseCoordinates.Y != buster.Coordinates.Y)
                        buster.Move(new Coordinates(buster.Coordinates.X, _gameState.BaseCoordinates.Y));
                    else
                        buster.Move(_gameState.BaseCoordinates);
                }
            }
            else
            {
                buster.Release();
                Console.Error.WriteLine("CarryingGhost -> Explore (dist to base: {0})",
                    buster.Coordinates.Distance(_gameState.BaseCoordinates));
                buster.State = new Explore(_gameState);
            }
        }
    }

    class ChaseEnemyCarryingGhost : State
    {
        private GameState _gameState;

        private Buster _enemy;

        private int _maxChasingTime;

        public ChaseEnemyCarryingGhost(GameState gameState, Buster enemy)
        {
            _gameState = gameState;
            _enemy = enemy;
            _maxChasingTime = 5;
        }

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("ChaseEnemyCarryingGhost");

            if (buster.Stunned)
            {
                Console.Error.WriteLine("ChaseEnemyCarryingGhost -> Stunned");
                buster.State = new Stunned(_gameState);
                buster.MakeMove(_gameState);
                return;
            }

            if (_gameState.ShouldStun(buster))
            {
                Buster target = _gameState.StunTarget(buster);
                buster.Stun(target);
                _enemy.CarryingGhost = false;
                buster.State = new Explore(_gameState);
                return;
            }

            if (!_enemy.Visible || !_enemy.CarryingGhost)
            {
                if (!_gameState.CarryingEnemyVisible())
                {
                    Console.Error.WriteLine("ChaseEnemyCarryingGhost -> Explore");
                    buster.State = new Explore(_gameState);
                    buster.MakeMove(_gameState);
                    return;
                }
                else
                    _enemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true);
            }
            if (_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true) != _enemy)
                _enemy = (Buster)_gameState.NearestEntity(buster, EntityTypes.Enemy, onlyCarryingGhost: true);

            if (_maxChasingTime == 0)
            {
                buster.Move(Coordinates.Random(buster));
                buster.State = new Explore(_gameState);
                return;
            }

            buster.Move(_enemy.Coordinates);
            _maxChasingTime--;
        }
    }

    class Stunned : State
    {
        public Stunned(GameState gameState)
        {
            _gameState = gameState;
        }

        private GameState _gameState;

        public override void Handle(Buster buster)
        {
            Console.Error.WriteLine("Stunned");
            if (!buster.Stunned)
            {
                Console.Error.WriteLine("Stunned -> Explore");
                buster.State = new Explore(_gameState);
                buster.MakeMove(_gameState);
            }
            else
                buster.Release();
        }
    }

    class Trap : State // TODO: n-1 ghost needed to win => wait before enemy base
    {
        public override void Handle(Buster buster)
        {
            throw new NotImplementedException();
        }
    }

    class Escort : State
    {
        public override void Handle(Buster buster)
        {
            throw new NotImplementedException();
        }
    }

    class Enemy : State
    {
        public override void Handle(Buster buster)
        {
        }
    }

    public class GameState
    {
        public int TurnNumber { get; set; }

        public int BustersPerPlayer { get; }

        public Dictionary<int, Buster> Busters { get; set; } = new Dictionary<int, Buster>();

        public Dictionary<int, Buster> EnemyBusters { get; set; } = new Dictionary<int, Buster>();

        public int GhostCount { get; }

        public Dictionary<int, Ghost> Ghosts { get; set; } = new Dictionary<int, Ghost>();

        public int TeamId { get; }

        private int _stunRange = 1760;

        public Map Map { get; set; }

        public Coordinates BaseCoordinates { get; set; }

        public GameState()
        {
            string line = Console.ReadLine();
            Console.Error.WriteLine(line);
            BustersPerPlayer = int.Parse(line); // the amount of busters you control

            line = Console.ReadLine();
            Console.Error.WriteLine(line);
            GhostCount = int.Parse(line); // the amount of ghosts on the map

            line = Console.ReadLine();
            Console.Error.WriteLine(line);
            TeamId = int.Parse(line);
            // if this is 0, your base is on the top left of the map, if it is one, on the bottom right

            Map = new Map();

            if (TeamId == 0)
                BaseCoordinates = new Coordinates(0, 0);
            else
                BaseCoordinates = new Coordinates(16000, 9000);
        }

        public bool EntityVisible(EntityTypes type)
        {
            IEntity[] entities;
            if (type == EntityTypes.Ghost)
            {
                entities = Ghosts.Values.ToArray();
                if (lowestHPCondition())
                    entities = entities.Where(ghost => ((Ghost)ghost).HealthPoints < 20).ToArray();
            }
            else if (type == EntityTypes.Enemy)
                entities = EnemyBusters.Values.ToArray();
            else
                throw new Exception("Invalid entity type");
            return Array.Exists(entities, entity => entity.Visible);
        }

        private bool UnchasedGhostVisible()
        {
            Ghost[] ghosts = Ghosts.Values.ToArray();
            if (lowestHPCondition())
                ghosts = ghosts.Where(ghost => ghost.HealthPoints < 20).ToArray();
            return Array.Exists(ghosts, ghost => ghost.Visible && ghost.ChasedByBusters.Count == 0);
        }

        public bool BustingEnemyVisible()
        {
            return Array.Exists(EnemyBusters.Values.ToArray(), enemy => enemy.Visible && enemy.TrappingGhost);
        }

        public bool CarryingEnemyVisible()
        {
            return Array.Exists(EnemyBusters.Values.ToArray(), enemy => enemy.Visible && enemy.CarryingGhost);
        }

        public bool GhostPositionRemembered()
        {
            Ghost[] ghosts = Ghosts.Values.ToArray();
            if (lowestHPCondition())
                ghosts = ghosts.Where(ghost => ghost.HealthPoints < 20).ToArray();
            return Array.Exists(ghosts, ghost => !ghost.Visible && ghost.StillThere);
        }

        public IEntity NearestEntity(Buster buster, EntityTypes type,
            bool onlyCapturingGhost = false, bool onlyCarryingGhost = false, bool stilThere = false)
        {
            if (!EntityVisible(type) && !stilThere)
                throw new Exception("No visible entities");
            IEntity[] entities;
            if (type == EntityTypes.Ghost)
            {
                if (stilThere)
                    entities = Ghosts.Values.Where(ghost => !ghost.Visible && ghost.StillThere).ToArray();
                else
                {
                    entities = Ghosts.Values.Where(ghost => ghost.Visible).ToArray();
                    if (UnchasedGhostVisible())
                        entities = entities.Where(ghost => ((Ghost)ghost).ChasedByBusters.Count == 0).ToArray();
                }
                if (lowestHPCondition())
                {
                    int minHP = entities.Min(ghost => ((Ghost)ghost).HealthPoints);
                    entities = entities.Where(ghost => ((Ghost)ghost).HealthPoints == minHP).ToArray();
                }
            }
            else if (type == EntityTypes.Enemy)
            {
                entities = EnemyBusters.Values.Where(enemy => enemy.Visible).ToArray();
                if (onlyCapturingGhost)
                    entities = entities.Where(enemy => ((Buster)enemy).TrappingGhost).ToArray();
                else if (onlyCarryingGhost)
                    entities = entities.Where(enemy => ((Buster)enemy).CarryingGhost).ToArray();
            }

            else
                throw new Exception("Invalid entity type");
            IEntity nearestEntity = entities[0];
            double minDistance = Single.PositiveInfinity;
            foreach (IEntity entity in entities)
            {
                double distance = buster.Coordinates.Distance(entity.Coordinates);
                if (distance < minDistance)
                {
                    minDistance = distance;
                    nearestEntity = entity;
                }
            }
            return nearestEntity;
        }

        public bool ShouldStun(Buster buster)
        {
            if (!EntityVisible(EntityTypes.Enemy) || buster.CanUseStun())
                return false;
            Buster nearestEnemy = (Buster)NearestEntity(buster, EntityTypes.Enemy);
            if (nearestEnemy.Stunned)
                return false;
            return buster.Coordinates.Distance(nearestEnemy.Coordinates) <= _stunRange;
        }

        public Buster StunTarget(Buster buster)
        {
            return (Buster)NearestEntity(buster, EntityTypes.Enemy);
        }

        public bool lowestHPCondition()
        {
            return TurnNumber < 50;
        }
    }

    public class Map
    {
        public List<Area> Areas { get; set; } = new List<Area>();

        public Map(int xSize = 16000, int ySize = 9000, int areaXSize = 1800, int areaYSize = 2000)
        {
            for (int i = areaXSize / 2; i < xSize; i += areaXSize)
            {
                for (int j = areaYSize / 2; j < ySize; j += areaYSize)
                {
                    Areas.Add(new Area(i, j));
                }
            }
        }

        public bool ExistsUnvisitedArea()
        {
            return Areas.Exists(area => !area.Visited);
        }

        public Area AreaToVisit(Buster buster)
        {
            if (!ExistsUnvisitedArea())
                throw new Exception("No unvisited areas");
            List<Area> unvisitedAreas = Areas.Where(area => !area.Visited).ToList();

            Area nearestArea = unvisitedAreas[0];
            double minDistance = Single.PositiveInfinity;
            Random random = new Random();
            int maxRandom = 5000;
            int distanceDisturbance = ((buster.Id + 1) * random.Next(maxRandom)) % maxRandom;
            foreach (Area area in unvisitedAreas)
            {
                double distance = buster.Coordinates.Distance(area) + distanceDisturbance;
                if (distance < minDistance)
                {
                    minDistance = distance;
                    nearestArea = area;
                }
            }
            return nearestArea;
        }
    }

    public class EntityFactory
    {
        public EntityFactory(GameState gameState)
        {
            _gameState = gameState;
        }

        private readonly GameState _gameState;

        public IEntity GetEntity(int entityType, int entityId)
        {

            if (entityType == (int)EntityTypes.Ghost)
            {
                if (!_gameState.Ghosts.ContainsKey(entityId))
                    _gameState.Ghosts[entityId] = new Ghost(entityId);
                return _gameState.Ghosts[entityId];
            }
            if (entityType == _gameState.TeamId)
            {
                if (!_gameState.Busters.ContainsKey(entityId))
                    _gameState.Busters[entityId] = new Buster(entityId, EntityTypes.Ally, new Explore(_gameState));
                return _gameState.Busters[entityId];
            }
            if (entityType != _gameState.TeamId)
            {
                if (!_gameState.EnemyBusters.ContainsKey(entityId))
                    _gameState.EnemyBusters[entityId] = new Buster(entityId, EntityTypes.Enemy, new Enemy());
                return _gameState.EnemyBusters[entityId];
            }
            throw new NullReferenceException("Invalid entity type");
        }
    }

    public class Coordinates
    {
        public Coordinates(int x, int y)
        {
            if (x < 0)
                x = 0;
            if (x > MaxX)
                x = MaxX;
            if (y < 0)
                y = 0;
            if (y > MaxY)
                y = MaxY;
            X = x;
            Y = y;
        }

        public static int MaxX { get; } = 16000;

        public static int MaxY { get; } = 9000;

        public int X { get; set; }

        public int Y { get; set; }

        public double Distance(Coordinates c)
        {
            return Math.Sqrt(Math.Pow(X - c.X, 2) + Math.Pow(Y - c.Y, 2));
        }

        public static Coordinates Random(Buster buster, int turnNumber = 1)
        {
            Random random = new Random();
            int randomX = (turnNumber * (buster.Id + 1) * random.Next(0, MaxX)) % MaxX;
            int randomY = (turnNumber * (buster.Id + 1) * random.Next(0, MaxY)) % MaxY;
            return new Coordinates(randomX, randomY);
        }

        public Coordinates Opposite(Coordinates nearestEnemyCoordinates)
        {
            return new Coordinates(nearestEnemyCoordinates.X - 2 * X, nearestEnemyCoordinates.Y - 2 * Y);
        }
    }

    public class Area : Coordinates
    {
        public bool Visited { get; set; }

        public Area(int x, int y) : base(x, y)
        {
            base.X = x;
            base.Y = y;
            Visited = false;
        }
    }

    public class Buster : IEntity
    {
        public Buster(int id, EntityTypes type, State state)
        {
            Id = id;
            Visible = true;
            Type = type;
            State = state;
            RoundsFromLastStun = 100;
            UsedRadar = false;
        }

        public State State { get; set; }

        public EntityTypes Type { get; set; }

        public int Id { get; }

        public Coordinates Coordinates { get; set; }

        public bool Visible { get; set; }

        public bool CarryingGhost { get; set; }

        public bool TrappingGhost { get; set; }

        public bool Stunned { get; set; }

        private Buster JustStunned { get; set; }

        public int CarriedGhostId { get; set; }

        public int RoundsFromLastStun { get; set; }

        public bool UsedRadar { get; private set; }

        public bool CanUseStun()
        {
            return RoundsFromLastStun <= 20;
        }

        public void Move(Coordinates destination)
        {
            Console.WriteLine("MOVE {0} {1}", destination.X, destination.Y);
        }

        public void Bust(Ghost target)
        {
            Console.WriteLine("BUST {0}", target.Id);
        }

        public void Release()
        {
            Console.WriteLine("RELEASE");
        }

        public void Stun(Buster target)
        {
            Console.WriteLine("STUN {0}", target.Id);
            target.Stunned = true;
            JustStunned = target;
            RoundsFromLastStun = 0;
        }

        public void Radar(GameState _gameState)
        {
            Console.WriteLine("RADAR");
            UsedRadar = true;
            foreach (Area area in _gameState.Map.Areas)
            {
                if (Coordinates.Distance(area) < 3500)
                    area.Visited = true;
            }
        }

        public void Update(Coordinates coordinates, int state, int value)
        {
            if (JustStunned != null && !JustStunned.Stunned)
                RoundsFromLastStun = 21;
            else
                JustStunned = null;
            Coordinates = coordinates;
            CarryingGhost = (state == 1);
            Stunned = (state == 2);
            TrappingGhost = (state == 3);
            CarriedGhostId = value;
            Visible = true;
        }

        public void MakeMove(GameState _gameState, bool newTurn = false)
        {
            Console.Error.WriteLine("Unexplored Areas: {0}", _gameState.Map.Areas.Where(area => !area.Visited).ToList().Count);
            Ghost[] ghostsStillThere = _gameState.Ghosts.Values.Where(ghost => ghost.StillThere).ToArray();
            foreach (Ghost ghost in ghostsStillThere)
            {
                if (Coordinates.Distance(ghost.Coordinates) < 1000)
                    ghost.StillThere = false;
            }

            foreach (Area area in _gameState.Map.Areas)
            {
                if (Coordinates.Distance(area) < 1000)
                    area.Visited = true;
            }

            State.Handle(this);
            if (newTurn)
                RoundsFromLastStun++;
        }
    }

    public class Ghost : IEntity
    {
        public Ghost(int id)
        {
            Id = id;
            Visible = true;
            ChasedByBusters = new HashSet<Buster>();
            StillThere = true;
        }

        public int Id { get; }

        public int NumberOfAttackingBusters;

        public Coordinates Coordinates { get; set; }

        public bool Visible { get; set; }

        public int HealthPoints { get; set; }

        public bool StillThere { get; set; }

        public void Update(Coordinates coordinates, int state, int value)
        {
            Coordinates = coordinates;
            NumberOfAttackingBusters = value;
            Visible = true;
            HealthPoints = state;
        }

        public HashSet<Buster> ChasedByBusters { get; set; }
    }

    class Player
    {
        private static void Main()
        {
            GameState gameState = new GameState();
            EntityFactory entityFactory = new EntityFactory(gameState);

            // game loop
            while (true)
            {
                gameState.TurnNumber++;
                string line = Console.ReadLine();
                Console.Error.WriteLine(line);
                int numberOfEntities = int.Parse(line); // the number of busters and ghosts visible to you,

                foreach (Ghost ghost in gameState.Ghosts.Values)
                    ghost.Visible = false;

                foreach (Buster enemyBuster in gameState.EnemyBusters.Values)
                    enemyBuster.Visible = false;

                for (int i = 0; i < numberOfEntities; i++)
                {
                    line = Console.ReadLine();
                    Console.Error.WriteLine(line);
                    string[] inputs = line.Split(' ');
                    int entityId = int.Parse(inputs[0]); // buster id or ghost id
                    int x = int.Parse(inputs[1]);
                    int y = int.Parse(inputs[2]); // position of this buster / ghost
                    Coordinates coordinates = new Coordinates(x, y);
                    int entityType = int.Parse(inputs[3]); // the team id if it is a buster, -1 if it is a ghost.
                    int state = int.Parse(inputs[4]); // For busters: 0=idle, 1=carrying a ghost, 2=stunned.
                    int value = int.Parse(inputs[5]); // For busters: Ghost id being carried. For ghosts: number of busters attempting to trap this ghost.

                    IEntity entity = entityFactory.GetEntity(entityType, entityId);
                    entity.Update(coordinates, state, value);
                }
                foreach (Buster buster in gameState.Busters.Values)
                {
                    buster.MakeMove(gameState, newTurn: true);
                    Console.Error.WriteLine();
                }
            }
        }
    }
}

// TODO: notice and use ghost's HP -> catch strong ghost after exploring 50%/75%/100% of area
// TODO: get ghost with lowest HP instead nearest one -- DONE
// TODO?: fix randomness in areaToVisit -- DONE
// TODO: use SCAN
// TODO: Attack enemy carrying ghost -- DONE